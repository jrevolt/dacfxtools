﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace JRevolt.DacFxTools
{
   public class Utils
   {
      public static object ParseEnum(Type type, string value)
      {
         if (Enum.TryParse(type, value, out var result)) return result;

         var search = value.ToLower().Replace("_", "");
         var found = Enum.GetNames(type).First(x => x.ToLower().Replace("_", "").Equals(search));
         return Enum.Parse(type, found ?? value);

      }

      public static int? ParseInt(string x)
      {
         return x != null ? int.Parse(x) : (int?) null;
      }

      public static IList<string> GetPropertiesAsList(object obj)
      {
         return
            obj.GetType().GetProperties()
               .Select(x => (name: x.Name, value: x.GetValue(obj)))
               .Select(x => $"{x.name}={x.value}")
               .ToList()
               .ToImmutableSortedSet();
      }

   }


}

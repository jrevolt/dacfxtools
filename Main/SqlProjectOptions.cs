﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.SqlServer.Dac;
using Microsoft.SqlServer.Dac.Model;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using CatalogCollation = Microsoft.SqlServer.Dac.Model.CatalogCollation;
using NonTransactedFileStreamAccess = Microsoft.SqlServer.Dac.Model.NonTransactedFileStreamAccess;
using ServiceBrokerOption = Microsoft.SqlServer.Dac.Model.ServiceBrokerOption;

namespace JRevolt.DacFxTools
{
   public class SqlProjectOptions
   {
      public string DSP { get; private set; } = "Microsoft.Data.Tools.Schema.Sql.Sql140DatabaseSchemaProvider";

      // <DefaultCollation>Slovak_CI_AS</DefaultCollation>
      // <ModelCollation>1051,CI</ModelCollation>

      public string DefaultCollation { get; [UsedImplicitly] private set; }

      public string ModelCollation { get; [UsedImplicitly] private set; }
      public bool AnsiNulls { get; private set; } = true;
      public bool AnsiPadding { get; private set; } = true;
      public bool AnsiWarnings { get; private set; } = true;
      public bool ArithAbort { get; private set; } = true;
      public bool ConcatNullYieldsNull { get; private set; } = true;
      public bool NumericRoundAbort { get; private set; } = false;
      public bool QuotedIdentifier { get; private set; } = true;

      public bool Trustworthy { get; private set; } = false;
      public bool IsEncryptionOn { get; private set; } = false;
      public bool IsBrokerPriorityHonored { get; private set; } = false;
      public bool EnableFullTextSearch { get; private set; } = true;
      public bool DatabaseChaining { get; private set; } = false;
      public bool RecursiveTriggersEnabled { get; private set; } = false;

      public ServiceBrokerOption ServiceBrokerOption { get; private set; } = ServiceBrokerOption.DisableBroker;
      public string DatabaseState { get; private set; } = "ONLINE";
      public string DatabaseAccess { get; private set; } = "MULTI_USER";

      //public QueryStoreDesiredState UpdateOptions { get; [UsedImplicitly] private set; }
      public ParameterizationOption Parameterization { get; private set; } = ParameterizationOption.Simple;
      public Containment Containment { get; private set; } = Containment.None;
      public string CompatibilityMode { get; [UsedImplicitly] private set; }
      public string DatabaseDefaultFulltextLanguage { get; [UsedImplicitly] private set; }
      public string DatabaseDefaultLanguage { get; [UsedImplicitly] private set; }
      public bool IsNestedTriggersOn { get; private set; } = true;
      public bool IsTransformNoiseWordsOn { get; [UsedImplicitly] private set; }
      public string TwoDigitYearCutoff { get; [UsedImplicitly] private set; }
      public NonTransactedFileStreamAccess NonTransactedFileStreamAccess { get; private set; } = NonTransactedFileStreamAccess.Off;

      public bool AutoClose { get; [UsedImplicitly] private set; }

      public bool TreatTSqlWarningsAsErrors { get; [UsedImplicitly] private set; }

      public string SuppressTSqlWarnings { get; [UsedImplicitly] private set; }

      public (TSqlModelOptions mopts, PackageOptions popts, TSqlObjectOptions oopts) ToOptions()
      {
         return (
            mopts: new TSqlModelOptions
            {
               Collation = DefaultCollation,
               CatalogCollation = CatalogCollation.SQL_Latin1_General_CP1_CI_AS,
               AnsiNullsOn = AnsiNulls,
               AnsiPaddingOn = AnsiPadding,
               AnsiWarningsOn = AnsiWarnings,
               ArithAbortOn = ArithAbort,
               ConcatNullYieldsNull = ConcatNullYieldsNull,
               NumericRoundAbortOn = NumericRoundAbort,
               QuotedIdentifierOn = QuotedIdentifier,
               Trustworthy = Trustworthy,
               WithEncryption = IsEncryptionOn,
               HonorBrokerPriority = IsBrokerPriorityHonored,
               FullTextEnabled = EnableFullTextSearch,
               DBChainingOn = DatabaseChaining,
               RecursiveTriggersOn = RecursiveTriggersEnabled,
               ServiceBrokerOption = ServiceBrokerOption,
               DatabaseStateOffline = DatabaseState.Equals("OFFLINE"),
               UserAccessOption
                  = DatabaseAccess.Contains("MULTI") ? UserAccessOption.Multiple
                  : DatabaseAccess.Contains("RESTRICTED") ? UserAccessOption.Restricted
                  : DatabaseAccess.Contains("SINGLE") ? UserAccessOption.Single
                  : throw new ArgumentException(DatabaseAccess),
               //QueryStoreDesiredState = UpdateOptions,
               ParameterizationOption = Parameterization,
               Containment = Containment,
               CompatibilityLevel = Utils.ParseInt(CompatibilityMode ?? "130"),
               DefaultFullTextLanguage = DatabaseDefaultFulltextLanguage,
               DefaultLanguage = DatabaseDefaultLanguage,
               NestedTriggersOn = IsNestedTriggersOn,
               TransformNoiseWords = IsTransformNoiseWordsOn,
               TwoDigitYearCutoff = (short?) Utils.ParseInt(TwoDigitYearCutoff ?? "2049"),
               NonTransactedFileStreamAccess = NonTransactedFileStreamAccess,
               AutoClose = AutoClose,
            },
            popts: new PackageOptions
            {
               TreatWarningsAsErrors = TreatTSqlWarningsAsErrors,
               IgnoreValidationErrors = SuppressTSqlWarnings?.Split(","),
            },
            oopts: new TSqlObjectOptions
            {
               AnsiNulls = AnsiNulls,
               QuotedIdentifier = QuotedIdentifier,
            }
         );
      }

      public SqlServerVersion GetSqlServerVersion(TSqlModelOptions opts)
      {
         var s = DSP
            .Replace("Microsoft.Data.Tools.Schema.Sql.", "")
            .Replace("DatabaseSchemaProvider", "");
         return Enum.Parse<SqlServerVersion>(s);
      }
   }
}

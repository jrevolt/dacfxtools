﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Xml;
using Autofac.AttributeExtensions;
using Castle.DynamicProxy.Internal;
using JRevolt.Injection;
using Microsoft.SqlServer.Dac;
using Microsoft.SqlServer.Dac.Model;
using Serilog;

namespace JRevolt.DacFxTools
{
   [SingleInstance]
   public class SqlProject
   {
      [Inject] private ILogger Log { get; }
      [Inject] private BuildOptions Options { get; }

      public SqlProjectOptions SqlProjectOptions { get; private set; }

      private ISet<string> IgnoredProperties { get; } = new HashSet<string>()
      {
         "Name",
         "Configuration",
         "Platform",
         "SchemaVersion",
         "ProjectVersion",
         "ProjectGuid",
         "OutputType",
         "OutputPath",
         "RootPath",
         "RootNamespace",
         "AssemblyName",
         "AssemblyNamDefaultFileStructure",
         "DeployToDatabase",
         "TargetFrameworkVersion",
         "TargetLanguage",
         "AppDesignerFolder",
         "SqlServerVerification",
         "IncludeCompositeObjects",
         "TargetDatabaseSet",
         "OuputPath",
         "BuildScriptName",
         "TreatWarningsAsErrors",
         "DebugType",
         "Optimize",
         "DefineDebug",
         "DefineTrace",
         "ErrorReport",
         "WarningLevel",
         "VisualStudioVersion",
         "SSDTExists",
         "DebugSymbols",
         "DefaultFileStructure",
         "GenerateCreateScript",
      };

      // private IDictionary<string,string> mapping = new Dictionary<string,string>()
      // {
      //    {nameof(SqlProjectOptions.DefaultCollation), nameof(TSqlModelOptions.Collation)},
      //    {nameof(SqlProjectOptions.AnsiPadding), nameof(TSqlModelOptions.AnsiPaddingOn)},
      //    {nameof(SqlProjectOptions.AnsiWarnings), nameof(TSqlModelOptions.AnsiWarningsOn)},
      //    {nameof(SqlProjectOptions.QuotedIdentifier), nameof(TSqlModelOptions.QuotedIdentifierOn)},
      // };

      internal SqlProjectOptions LoadSqlProject()
      {
         var sqlproj = Directory.EnumerateFiles(Options.Directory, "*.sqlproj").First();
         Log.Information("Loading {0}", sqlproj);

         var doc = new XmlDocument();
         doc.Load(sqlproj);
         var ns = new XmlNamespaceManager(doc.NameTable);
         ns.AddNamespace("x", doc.DocumentElement.GetAttribute("xmlns"));

         var srcProperties =
            doc.SelectNodes("//x:PropertyGroup/*", ns).Cast<XmlElement>()
               .Where(x => !IgnoredProperties.Contains(x.Name))
               .Select(x => (name: x.Name, value: x.InnerText))
               .ToList();

         var dstProperties =
            typeof(SqlProjectOptions).GetProperties()
               .ToImmutableDictionary(x => x.Name);

         var unsupported =
            srcProperties
               .Where(x => !dstProperties.ContainsKey(x.name))
               .ToList();
         unsupported.ToList().ForEach(x =>
         {
            Log.Warning("Unuspported SQLProject property: {0}={1}", x.name, x.value);
         });

         var opts = new SqlProjectOptions();

         srcProperties
            .Where(x => dstProperties.ContainsKey(x.name))
            .Select(x => (x.name, x.value, property: dstProperties[x.name]))
            .ToList().ForEach(x =>
            {
               var p = x.property;

               var type = p.PropertyType.IsNullableType()
                  ? p.PropertyType.GetGenericArguments().First()
                  : p.PropertyType;

               object parsed;

               parsed
                  = x.value == null ? null
                  : type.IsAssignableFrom(typeof(bool)) ? bool.Parse(x.value)
                  : type.IsEnum ? Utils.ParseEnum(type, x.value)
                  : x.value;

               Log.Information("{0}.{1}={2}", p.DeclaringType?.Name, p.Name, parsed);
               p.SetValue(opts, parsed);
            });

         var (mopts, popts, oopts) = opts.ToOptions();

         Options.ModelOptions = mopts;
         Options.PackageOptions = popts;
         Options.ObjectOptions = oopts;

         return opts;
      }
   }
}

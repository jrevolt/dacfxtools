﻿using System;
using System.Reflection;
using Autofac;
using Autofac.AttributeExtensions;
using Autofac.Core;
using JetBrains.Annotations;
using JRevolt.Injection;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace JRevolt.DacFxTools
{
  internal static class InternalExtensions
  {
    public static ContainerBuilder RegisterConfiguration(this ContainerBuilder builder, IConfiguration cfg)
    {
      builder.RegisterInstance(cfg).As<IConfiguration>();
      return builder;
    }

  }

  [SingleInstance]
  public class LoggerResolver : IDynamicServiceResolver<ILogger>
  {
    public void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result)
    {
      result = Log.ForContext(registration.Activator.LimitType);
    }
  }

}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Autofac.AttributeExtensions;
using JetBrains.Annotations;
using JRevolt.DacFxTools.Ssdt;
using JRevolt.Injection;
using Microsoft.SqlServer.Dac;
using Microsoft.SqlServer.Dac.CodeAnalysis;
using Microsoft.SqlServer.Dac.Deployment;
using Microsoft.SqlServer.Dac.Model;
using Serilog;

namespace JRevolt.DacFxTools
{
   [SingleInstance(Name = "build:options")]
   internal class BuildOptions : ICommandOptions
   {
      [Required] public string Directory { get; [UsedImplicitly] set; }
      [Required] public string Output { get; [UsedImplicitly] set; }
      public string Include { get; [UsedImplicitly] set; } = ".*";
      public string Exclude { get; [UsedImplicitly] set; } = "(bin|obj)/.*";
      public string Scripts { get; [UsedImplicitly] set; } = "(scripts|migration)/.*";

      public string Name { get; [UsedImplicitly] set; }
      public string Version { get; [UsedImplicitly] set; }

      public TSqlModelOptions ModelOptions { get; [UsedImplicitly] set; }

      public PackageOptions PackageOptions { get; [UsedImplicitly] set; }

      public TSqlObjectOptions ObjectOptions { get; [UsedImplicitly] set; }
   }

   [SingleInstance(Name = "build")]
   public class Build : ICommand
   {
      [Inject] private ILogger Log { get; }
      [Inject] private BuildOptions Options { get; }
      [Inject] private SqlProject SqlProject { get; }

      public void Run()
      {
         var cfg = Options;

         Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

         var pmeta = new PackageMetadata
         {
            Name = cfg.Name,
            // valid version is n.n.n.n, strip valid part from full semver provided
            Version = Regex.Replace(cfg.Version, "^((\\d+\\.){1,3}(\\d+)?).*", "$1"),
            // and save full semver in description
            Description = cfg.Version,
         };

         var opts = SqlProject.LoadSqlProject();

         Log.Debug("TSqlModelOptions = {@0}", Options.ModelOptions);
         Log.Debug("PackageOptions = {@0}", Options.PackageOptions);
         Log.Debug("TSqlObjectOptions = {@0}", Options.ObjectOptions);

         Options.PackageOptions.DeploymentContributors = new[]
         {
            new DeploymentContributorInformation
            {
               ExtensionId
                  = typeof(DacFxToolsContributor).GetCustomAttribute<ExportDeploymentPlanModifierAttribute>()?.Id
                    ?? throw new ApplicationException("deployment contributor?")
            },
         };

         Options.PackageOptions.RefactorLogPath = Directory
            .GetFiles(cfg.Directory, "*.refactorlog", SearchOption.TopDirectoryOnly)
            .FirstOrDefault();

         var sqlServerVersion = opts.GetSqlServerVersion(Options.ModelOptions);
         using (var model = new TSqlModel(sqlServerVersion, Options.ModelOptions))
         {
            var sqlfiles = Directory.GetFiles(cfg.Directory, "*.sql", SearchOption.AllDirectories).ToList();
            sqlfiles.Sort(StringComparer.Ordinal);
            foreach (var pathname in sqlfiles)
            {
               var rel = Path.GetRelativePath(cfg.Directory, pathname).Replace('\\', '/');

               // includes
               if (!Regex.IsMatch(rel, cfg.Include)) continue;

               // excludes
               if (Regex.IsMatch(rel, cfg.Exclude)) continue;

               // scripts are not compiled, only attached
               if (Regex.IsMatch(rel, cfg.Scripts)) continue;

               Log.Information("Loading {0}", rel);
               var sql = LoadSql(pathname);
               model.AddOrUpdateObjects(sql, pathname, Options.ObjectOptions);
            }

            foreach (var o in model.GetObjects(DacQueryScopes.UserDefined))
            {
               Log.Information("{0} : {1} = {2}", o.Name, o.ObjectType.Name, o);
            }

            var analysis = new CodeAnalysisServiceFactory().CreateAnalysisService(model.Version);
            var result = analysis.Analyze(model);
            PrintProblemsAndValidationErrors(model, result);

            DacPackageExtensions.BuildPackage(cfg.Output, model, pmeta, Options.PackageOptions);
            Log.Information("Built {0}", Path.GetFullPath(cfg.Output));

            Log.Information("Including scripts {0}", cfg.Scripts);
            using (var zip = ZipFile.Open(cfg.Output, ZipArchiveMode.Update))
            {
               var scripts = Directory.EnumerateFiles(cfg.Directory, "*.sql", SearchOption.AllDirectories);
               foreach (var f in scripts)
               {
                  var rel = Path.GetRelativePath(cfg.Directory, f).Replace('\\', '/');
                  if (!Regex.IsMatch(rel, cfg.Scripts)) continue;
                  var e = zip.GetEntry(rel);
                  e?.Delete();
                  Log.Information("Adding {0}", rel);
                  zip.CreateEntryFromFile(f, rel);
               }
            }

            Log.Information("Finished {0}", Path.GetFullPath(cfg.Output));

            var loaded = DacPackage.Load(cfg.Output);
            Log.Information("Package version: {0} ({1})", loaded.Version, loaded.Description);
         }

         string LoadSql(string file)
         {
            var encodings = new[]
            {
               Encoding.UTF8,
               Encoding.GetEncoding("Windows-1250"),
               Encoding.GetEncoding("ISO-8859-2"),
            };
            var dflt = File.ReadAllText(file);
            var valid = encodings
               .Select(enc => File.ReadAllText(file, enc))
               .Aggregate(true, (current, candidate) => current & dflt.Equals(candidate));
            if (!valid) Log.Warning("File must be UTF-8-BOM encoded {0}", file);

            return dflt;
         }

         void PrintProblemsAndValidationErrors(TSqlModel model, CodeAnalysisResult analysisResult)
         {
            Console.WriteLine("-----------------");
            Console.WriteLine("Outputting validation issues and problems");
            foreach (var issue in model.Validate())
            {
               Console.WriteLine("\tValidation Issue: '{0}', Severity: {1}",
                  issue.Message,
                  issue.MessageType);
            }

            foreach (var problem in analysisResult.Problems)
            {
               Console.WriteLine(
                  "\tCode Analysis Problem: '{0}', Severity: {1}, Source: {2}, StartLine/Column [{3},{4}]",
                  problem.ErrorMessageString,
                  problem.Severity,
                  problem.SourceName,
                  problem.StartLine,
                  problem.StartColumn);
            }

            Console.WriteLine("-----------------");
         }
      }


   }
}

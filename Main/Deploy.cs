﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Autofac.AttributeExtensions;
using JetBrains.Annotations;
using JRevolt.Configuration;
using JRevolt.Injection;
using Microsoft.Data.SqlClient;
using Microsoft.SqlServer.Dac;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;

namespace JRevolt.DacFxTools
{
   [SingleInstance(Name = "deploy:options")]
   [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
   internal class DeployOptions : ICommandOptions
   {
      [Required] public string Server { get; [UsedImplicitly] set; }
      [Required] public string Database { get; [UsedImplicitly] set; }
      [Required] public string Username { get; [UsedImplicitly] set; }
      [Required] public string Password { get; [UsedImplicitly] set; }
      public string ConnectionParameters { get; [UsedImplicitly] set; }

      [Required] public string DacPac { get; [UsedImplicitly] set; }

      public Dictionary<string, string> SqlCommandVariables { get; [UsedImplicitly] set; }

      public bool DryRun { get; [UsedImplicitly] set; }

      public DacDeployOptions DacDeployOptions { get; [UsedImplicitly] set; } = new DacDeployOptions();

      public PublishOptions PublishOptions { get; [UsedImplicitly] set; } = new PublishOptions();

      internal string ConnectionString => string.Join(';'
         , $"Data Source={Server}"
         , $"User ID={Username}"
         , $"Password={Password}"
         , ConnectionParameters);

   }

   [SingleInstance(Name = "deploy")]
   internal class Deploy : ICommand
   {
      [Inject] private ILogger Log { get; }
      [Inject] private DeployOptions Options { get; }

      public void Run()
      {
         Log.Debug("options: {@0}", Options);

         var cfg = Options;
         var poptions = Options.PublishOptions;
         var doptions = Options.DacDeployOptions;

         if (cfg.SqlCommandVariables != null)
            foreach (var (key, value) in cfg.SqlCommandVariables)
               doptions.SqlCommandVariableValues.Add(key, value);

         Log.Debug("Configured deployment options: {@0}", doptions);

         // File.WriteAllText(cfg.DacPac + ".options.json", JsonConvert.SerializeObject(options, Newtonsoft.Json.Formatting.Indented));

         var dacpac = Path.GetFullPath(cfg.DacPac);
         var cwd = Path.GetFullPath(Directory.GetCurrentDirectory());
         var tmpdir = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()));

         try
         {
            Log.Information("Working directory: {0}", Directory.GetCurrentDirectory());
            Directory.SetCurrentDirectory(tmpdir.FullName);

            Log.Debug("Extracing {0} into {1}", dacpac, tmpdir);
            var tmpdacpac = Path.Combine(tmpdir.FullName, Path.GetFileName(dacpac));
            ZipFile.ExtractToDirectory(dacpac, tmpdir.FullName);
            File.Copy(dacpac, tmpdacpac);

            Log.Debug("Loading {0}", tmpdacpac);
            using (var dp = DacPackage.Load(tmpdacpac))
            {
               Log.Information("Package version: {0}", dp.Version);

               if (dp.Version != null) doptions.SqlCommandVariableValues.Add("DacpacVersion", dp.Version.ToString());
               if (dp.Description != null) doptions.SqlCommandVariableValues.Add("DacpacDescription", dp.Description);

               var con = cfg.ConnectionString;
               Log.Information("Deploying using connection: {0}", con);

               var services = new DacServices(con);
               services.Message += (sender, args) =>
               {
                  var lvl = args.Message.MessageType switch
                  {
                     DacMessageType.Error => LogEventLevel.Error,
                     DacMessageType.Warning => LogEventLevel.Warning,
                     _ => LogEventLevel.Information
                  };
                  Log.Write(lvl, "[{0}] {1}", args.Message?.Prefix ?? "dacfxtools", args.Message?.Message);
               };

               if (cfg.DryRun)
               {
                  var sql = services.GenerateDeployScript(dp, cfg.Database, doptions);
                  if (poptions.GenerateDeploymentScript)
                  {
                     Log.Information("Writing {0}", poptions.DatabaseScriptPath);
                     File.WriteAllText(poptions.DatabaseScriptPath, sql);
                  }

               }
               else
               {
                  poptions.DeployOptions = doptions;
                  services.Publish(dp, cfg.Database, poptions);
               }
            }

            Log.Information("Deployment completed successfully ({0})", Path.GetFileName(dacpac));
         }
         catch (Exception e)
         {
            Log.Error("Deployment failed: {0}", e.Message);
            throw;
         }
         finally
         {
            Directory.SetCurrentDirectory(cwd);
            tmpdir?.Delete(true);
         }
      }
   }
}

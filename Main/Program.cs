﻿using System;
using Autofac;
using JRevolt.Configuration;
using JRevolt.DacFxTools.Ssdt;
using JRevolt.Injection;
using Serilog;

namespace JRevolt.DacFxTools
{
   internal class Program
   {
      internal static void Main()
      {
         var cfg = new ConfigurationLoader<Program>().Load().Build();
         Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(cfg).CreateLogger();
         var meta = new Metadata().Scan(typeof(Program).Assembly);
         using var container = new ContainerBuilder()
            .RegisterConfiguration(cfg)
            .UseInjector(meta).Build().EnableInjectorSupport();
         container.Resolve<Main>().Run();
      }
   }
}

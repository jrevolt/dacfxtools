﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using Autofac;
using Autofac.AttributeExtensions;
using JRevolt.Injection;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace JRevolt.DacFxTools
{
  [SingleInstance]
  internal class Main
  {
    [Inject] private IConfiguration Cfg { get; }
    [Inject] private ILifetimeScope Scope { get; }

    internal void Run()
    {
      var cmdlist = GetCommandFromCommandLine() ?? Cfg["command"] ?? "help";
      cmdlist.Split(",").ToList().ForEach(cmdname =>
      {
         Log.Debug("Command: {0}", cmdname);
         var options = Scope.ResolveKeyed<ICommandOptions>($"{cmdname}:options");
         Cfg.Bind(cmdname, options);
         Cfg.Bind(options);
         var ctx = new ValidationContext(options);
         ICollection<ValidationResult> result = new List<ValidationResult>();
         var valid = Validator.TryValidateObject(options, ctx, result);
         if (!valid)
         {
            result.ToList().ForEach(x => Log.Error(x.ErrorMessage));
            throw new ValidationException("Invalid input!");
         }
         var cmd = Scope.ResolveNamed<ICommand>(cmdname);
         cmd.Run();
      });
    }

    internal string GetCommandFromCommandLine()
    {
      var args = Environment.GetCommandLineArgs().Skip(1);
      var candidate = args.DefaultIfEmpty(null).First();
      if (candidate == null) return null;
      return Regex.IsMatch(candidate, "^[^-/][^=]+$") ? candidate : null;
    }
  }

  internal interface ICommand
  {
    void Run();
  }

  internal interface ICommandOptions
  {
  }
}

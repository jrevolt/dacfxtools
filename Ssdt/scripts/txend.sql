﻿print 'Finishing transaction'
go

BEGIN
	if xact_state()=1
	begin
		declare @failed int

		if exists (select 1 from m.deploymentlog where level='ERR')
		begin
			exec m.saferollback Deployment;
			set @failed=1
		end

		-- always commit, all that needed to be rolled back in case of error has already been
		COMMIT

		if @failed is null
			print 'Transaction committed.'
		else
			print 'Transaction rolled back!'
	end
	else if xact_state()=-1
	begin
		rollback;
		print 'Transaction rolled back'
	end
	else
	begin
		print 'No transaction to commit or roll back.'
	end
END
GO

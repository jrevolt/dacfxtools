﻿print 'Loading bootstrap'
go

if not exists (select name from sys.schemas where name='m')
    exec('create schema [m]');
go

if object_id('m.ExecuteBase64SQL') is not null drop procedure m.ExecuteBase64SQL
go
create procedure m.ExecuteBase64SQL(@encoded varchar(max)) as
begin
	declare @decoded varbinary(max), @sql nvarchar(max)
	set @decoded = cast('' as xml).value('xs:base64Binary(sql:variable("@encoded"))', 'varbinary(max)')
	set @sql = convert(nvarchar(max), @decoded)
	--print @sql
	exec sp_executesql @sql
end
go


﻿print 'Initializing [m] schema'
go

-- init schema
if not exists (select name from sys.schemas where name='m')
    exec('create schema [m]');

-- init tables
if object_id('m.sqlfiles') is null
	create table m.sqlfiles(
		type sysname, name sysname, phase sysname, id int identity(1,1), data nvarchar(max),
		status varchar(32), --
		started datetime, duration int, error int, result int, txstatus int, msg nvarchar(max));
if object_id('m.version') is null
    create table m.version(id int identity(1,1), type sysname, name sysname, deployed datetime);
if object_id('m.deploymentlog') is null
	create table m.deploymentlog(tstamp datetime, level char(3), msg nvarchar(max))
if object_id('m.variables') is null
    create table m.variables(name sysname primary key, value nvarchar(max))
go

-- cleanup old sqlfiles not associated with already deployed version
delete m.sqlfiles
from m.sqlfiles f full outer join m.version v on f.type=v.type and f.name=v.name
where v.id is null
-- reset deployment log
delete from m.deploymentlog
-- reset variables
delete from m.variables
go

-- advanced cleanup
if object_id('m.cleanup') is not null drop procedure m.cleanup
go
create procedure m.cleanup as
begin
	begin try
		declare @cleanup table (name sysname, type sysname)
		insert into @cleanup (name, type)
			select o.name, o.type
			from sys.objects o inner join sys.schemas s on o.schema_id=s.schema_id
			where (s.name='m' and o.type in ('P','FN') and o.name not in ('cleanup', 'CleanupAndRaiseErrorOnFailure', 'ExecuteBase64SQL'))
			   or (s.name='m' and o.type in ('U') and o.name not in ('sqlfiles', 'version', 'deploymentlog', 'variables'))

		declare @name sysname
		declare @type sysname
		while exists (select * from @cleanup)
		begin
			select top 1 @name=name, @type=type from @cleanup
			if @type='P' exec('drop procedure m.'+@name)
			if @type='FN' exec('drop function m.'+@name)
			if @type='U' exec('drop table m.'+@name)
			delete from @cleanup where name=@name and type=@type
		end
	end try
	begin catch
		print 'Error cleaning up. Ignoring...'
	end catch
end
go

exec m.cleanup
go



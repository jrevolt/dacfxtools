﻿print 'Starting transaction'
go

BEGIN TRANSACTION
-- to support rollback in m.saferollback() procedure, use savepoints
SAVE TRANSACTION Deployment
GO

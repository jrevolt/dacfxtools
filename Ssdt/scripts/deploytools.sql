﻿print 'Loading deployment tools'
go

create procedure m.AddMigrationScript(@type sysname, @name sysname, @phase sysname, @encoded varchar(max)) as
begin
    if not exists (select 1 from m.version where type=@type and name=@name)
        begin
            declare @decoded varbinary(max), @sql nvarchar(max)
            set @decoded = cast('' as xml).value('xs:base64Binary(sql:variable("@encoded"))', 'varbinary(max)')
            set @sql = convert(nvarchar(max), @decoded)
            insert into m.sqlfiles (type, name, phase, data) values (@type, @name, @phase, @sql)
        end
end
go

create procedure m.setfail(@msg nvarchar(max)) as
begin
	if xact_state()=-1 throw 50000, 'Uncommittable transaction!', 1;
	insert into m.deploymentlog (tstamp, level, msg) values (current_timestamp, 'ERR', @msg)
end
go

create procedure m.fail(@msg nvarchar(max)) as
begin
	exec m.setfail @msg
	;throw 50000, @msg, 1;
end
go

create procedure m.log(@msg nvarchar(max)) as
begin
	print '  | '+@msg
end
go

create procedure m.saveresult(@id int, @status varchar(32), @started datetime, @error int, @result int, @msg nvarchar(max) = '') as
begin
	declare @type sysname, @name sysname
	select @type=type, @name=name from m.sqlfiles where id=@id
	print formatmessage('  +[%s], result=%d, error=%d, txstatus=%d, msg="%s"',@status, @result, @error, xact_state(), @msg)
	update m.sqlfiles
	set status=@status, started=@started, duration=datediff(ms, @started, current_timestamp), error=@error, result=@result, txstatus=xact_state(), msg=@msg
	where id=@id
end
go


create procedure m.saferollback(@txname sysname) as
begin
	declare @results table (id int, status varchar(32), started datetime, duration int, error int, result int, txstatus int, msg nvarchar(max))
	insert into @results (id, status, started, duration, error, result, txstatus, msg)
		select id, status, started, duration, error, result, txstatus, msg from m.sqlfiles

	declare @deploymentlog table (tstamp datetime, level char(3), msg nvarchar(max))
	insert into @deploymentlog (tstamp, level, msg)
		select tstamp, level, msg from m.deploymentlog


	if xact_state()=1 rollback transaction @txname
	else return

	update r1
	set r1.status=r2.status, r1.started=r2.started, r1.duration=r2.duration, r1.error=r2.error, r1.result=r2.result, r1.txstatus=r2.txstatus, r1.msg=r2.msg
	from m.sqlfiles r1 inner join @results r2 on r1.id=r2.id

	delete from m.deploymentlog
	insert into m.deploymentlog select tstamp, level, msg from @deploymentlog
end
go

create procedure m.skip(@msg nvarchar(max) = 'subsequent steps marked as skipped') as
begin
	declare @id int, @type sysname, @name sysname, @phase sysname
	-- detect current step
	select @id=id, @type=type, @name=name, @phase=phase from m.sqlfiles where status='executing'
	-- mark all further steps for a given type/name/phase as skipped
	update m.sqlfiles set status='skip'
	where type=@type and name=@name and status is null
	exec m.log @msg
end
go

create procedure m.SkipOnMissingColumn(@table sysname, @column sysname) as
begin
	if col_length(@table, @column) is null
	begin
		declare @msg nvarchar(max) = formatmessage('Skipping. Column does not exist: %s.%s', @table, @column)
		exec m.skip @msg
	end
end
go

create procedure m.SkipOnMissingTable(@table sysname) as
begin
	if object_id(@table) is null
	begin
		declare @msg nvarchar(max) = formatmessage('Skipping. Table does not exist: %s', @table)
		exec m.skip @msg
	end
end
go

create procedure m.SkipOnMissingVersion(@type sysname, @name sysname) as
begin
	if not exists (select 1 from m.version where type=@type and name like @name+'%' and deployed is not null)
	begin
		declare @msg nvarchar(max) = formatmessage('Skipping. Version not yet deployed: %s/%s', @type, @name)
		exec m.skip @msg
	end
end
go

create procedure m.FailOnMissingVersion(@type sysname, @name sysname) as
begin
	if not exists (select 1 from m.version where type=@type and name=@name and deployed is not null) exec m.fail 'Required version not yet deployed'
end
go

create procedure m.UpdateSchemaVersion as
begin
    declare @ver nvarchar(max)
    declare @desc nvarchar(max)
    select @ver = value from m.variables where name = 'DacpacVersion'
    select @desc = value from m.variables where name = 'DacpacDescription'
    if @ver is null set @ver = '0.0.1'
    if @desc is null set @desc = ''
    declare @versionstring nvarchar(max) = @desc --formatmessage('%s (%s)', @ver, @desc)
    print 'Schema version: '+@versionstring
    insert into m.version (type, name, deployed) values ('schema', @desc, current_timestamp)
end
go


create procedure m.dophase(@phase sysname) as
begin
    declare @msg nvarchar(max)

	if @phase='predeploy'	print '### BEGIN PREDEPLOY ###'
	if @phase='postdeploy'	print '### BEGIN POSTDEPLOY ###'

	print 'Processing '+@phase+' migrations...'

  declare @cleandeploy bit = 0
  if not exists (select 1 from m.version) set @cleandeploy=1

  -- special case: initial clean deployment
  if @cleandeploy=1
  begin
    set @msg='Initial clean deployment: skip all migrations.'
    if @phase='postdeploy' set @msg=@msg + ' (Except those explicitly marked as *.noskip.sql.)'
    print @msg
    update m.sqlfiles set status='skip', msg='initial clean deployment'
    where phase=@phase and (phase='predeploy' or name not like '%.noskip.sql')
  end

	-- regular migration
	declare @failed int
	begin try
		save transaction @phase

		declare @results table (id int, tstamp datetime, error int, result int, txstatus int, msg nvarchar(max))
		declare @versions table (type sysname, name sysname, deployed datetime)

		declare @migrations table (id int identity(1,1), type sysname, name sysname)
		insert into @migrations (type, name)
			select type, name
			from m.sqlfiles
			group by type, name
			order by name
		while (exists (select 1 from @migrations))
		begin
			declare @id int, @type sysname, @name sysname
			select top 1 @id=id, @type=type, @name=name from @migrations
			delete from @migrations where id=@id

			-- Check currently deployed version
			declare @deployed sysname
			select @deployed = (select max(name) from m.version where type=@type)
			if @name<=@deployed
			begin
				print formatmessage('Already deployed: %s (%s)', @name, @type)
				continue
			end

			-- Resolve latest available version. This is the migration target.
			declare @latest sysname
			select @latest=(select max(name) from m.sqlfiles where type=@type)

			-- Check minimimum requirements.
			-- Older deployments will fail to migrate and must be first upgraded using intermediate releases.
			declare @minimum sysname
			select @minimum = (select min(name) from m.sqlfiles where type=@type)

			if @deployed<@minimum
			begin
				set @failed=1
				set @msg=formatmessage(
					'Cannot migrate %s/%s. Deployed version is too old. Required minimum: %s. Found: %s.',
					@type, @name, @minimum, @deployed)
				print @msg
				exec m.fail @msg
				--throw
			end

			print formatmessage('Processing migration: %s (%s)', @name, @type)
			begin
				begin try
					save transaction @name
					declare @sqltable table (id int, data nvarchar(max))
					insert into @sqltable (id, data)
						select id, data from m.sqlfiles
						where type=@type and name=@name and phase=@phase and (status is null or status <> 'skip')
						order by id;

					update m.sqlfiles set status='skipped', started=current_timestamp, duration=0
					where phase=@phase and type=@type and name=@name and status='skip'

					while (exists (select 1 from @sqltable))
					begin
						declare @sqlid int, @data nvarchar(max)
						select top 1 @sqlid=id, @data=data from @sqltable
						delete from @sqltable where id=@sqlid

						declare @started datetime = current_timestamp, @error int, @result int, @txstatus int
						if exists (select 1 from m.sqlfiles where id=@sqlid and status='skip')
						begin
						  set @msg=(select msg from m.sqlfiles where id=@sqlid)
							exec m.saveresult @id=@sqlid, @status='skipped', @started=@started, @error=@error, @result=@result, @msg=@msg
							continue
						end
						begin try
							--flag @sqlid as being executed
							update m.sqlfiles set status='executing' where id=@sqlid

							--### ACTUAL STUFF HAPPENS HERE! ###--
							exec @result = sp_executesql @data

							select @error=@@error, @txstatus=xact_state()
							exec m.saveresult @id=@sqlid, @status='completed', @started=@started, @error=@error, @result=@result
						end try
						begin catch
							select @error=@@error, @txstatus=xact_state(), @msg=error_message(), @failed=1
							exec m.saveresult @id=@sqlid, @status='error', @started=@started, @error=@error, @result=@result, @msg=@msg
							;throw
						end catch
					end
				end try
				begin catch
					exec m.saferollback @name
				end catch

				if @phase='postdeploy'
				begin
					insert into m.version (type, name, deployed) values (@type, @name, current_timestamp)
				end
			end
		end
	end try
	begin catch
		exec m.saferollback @phase
	end catch

    if @failed is not null
	begin
		-- Predeploy failures are reported as exceptions to abort deployment sequence and avoid further deployment steps.
		if @phase='predeploy'	exec m.fail 'Migration failed! '
		-- Postdeploy failures are reported by a flag object. Exception would prevent important postprocessing steps from
    -- being executed.
		if @phase='postdeploy'	exec m.setfail 'Migration failed!'
	end

	if @phase='predeploy'	print '### END PREDEPLOY ###'
	if @phase='postdeploy'	print '### END POSTDEPLOY ###'

    if @phase='postdeploy' exec m.UpdateSchemaVersion
end
go

if object_id('m.CleanupAndRaiseErrorOnFailure') is not null drop procedure m.CleanupAndRaiseErrorOnFailure
go
create procedure m.CleanupAndRaiseErrorOnFailure as
begin
	declare @msg nvarchar(max)
	begin try
		select top 1 @msg=msg from m.deploymentlog where level='ERR' order by tstamp
		exec m.cleanup
		drop procedure m.cleanup
	end try
	begin catch
	end catch
	if @msg is not null throw 50000, @msg, 1;
end
go

﻿-- only insert new files for as yet undeployed type/version
if not exists (select 1 from m.version where type=@type and name=@name)
begin
    insert into m.sqlfiles (type, name, phase, data) values (@type, @name, @phase, @data)
end

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace JRevolt.DacFxTools.Ssdt
{
   public class Utils
   {
      internal static List<string> SplitSqlBatches(string sql)
      {
         const RegexOptions opts = RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase;
         var sqlbatches = Regex.Split(sql, "^GO\\s*\\r?$", opts);
         return sqlbatches
            .Where(x => !string.IsNullOrWhiteSpace(x))
            .Select(x => x.Trim())
            .ToList();
      }

      internal static string Encode(string content)
      {
         var bytes = Encoding.Unicode.GetBytes(content);
         var encoded = Convert.ToBase64String(bytes);
         return encoded;
      }

      internal static byte[] Compress(byte[] data)
      {
         using (var compressedStream = new MemoryStream())
         using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
         {
            zipStream.Write(data, 0, data.Length);
            zipStream.Close();
            return compressedStream.ToArray();
         }
      }

      // static string CompressEncode(string content)
      // {
      //    var bytes = Encoding.Unicode.GetBytes(content);
      //    var compressed = Compress(bytes);
      //    var encoded = Convert.ToBase64String(compressed);
      //    return encoded;
      // }

      internal static string GetMigrationScripts(string migration, string phase)
      {
         const RegexOptions opts = RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase;
         var sql = Regex
            .Match(migration, "--<" + phase + ">--(.*)--</" + phase + ">--", opts)
            .Groups[1].Value;
         if (string.IsNullOrWhiteSpace(sql)) sql = "";
         return sql;
      }

   }
}

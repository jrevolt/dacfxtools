﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Dac.Deployment;
using Microsoft.SqlServer.Dac.Extensibility;

namespace JRevolt.DacFxTools.Ssdt
{
   [ExportDeploymentPlanModifier("JRevolt.DacFxTools.Ssdt.DacFxToolsContributor", "1.0.0.0")]
   public class DacFxToolsContributor : DeploymentPlanModifier
   {
      protected override void OnExecute(DeploymentPlanContributorContext context)
      {
         var assembly = Assembly.GetExecutingAssembly();
         var version = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();

         Log($"Starting... (JRevolt.DacFxTools version {version?.InformationalVersion})");
         ConfigureDeploymentPlan(context);
         ReportAllDrops(context);
      }

      private void ConfigureDeploymentPlan(DeploymentPlanContributorContext context)
      {
         Log("Configuring deployment plan...");

         const string versionVariable = "DacpacVersion";
         context.Options.SqlCommandVariableValues.TryGetValue(versionVariable, out var dacpacVersion);
         dacpacVersion ??= "0.0.1";
         context.Options.SqlCommandVariableValues[versionVariable] = dacpacVersion;

         var ph = context.PlanHandle;

         // find predeploy step
         var predeploy = context.PlanHandle.Head;
         while (predeploy != null && !(predeploy is BeginPreDeploymentScriptStep))
         {
            predeploy = predeploy.Next;
         }

         // find postdeploy step
         var postdeploy = context.PlanHandle.Head;
         while (postdeploy != null && !(postdeploy is BeginPostDeploymentScriptStep))
         {
            postdeploy = postdeploy.Next;
         }

         var bootstrap = new DeploymentScriptStep(Load("scripts/bootstrap.sql"));
         var mschema = new DeploymentScriptStep(LoadAsBase64Sql("scripts/mschema.sql"));
         var variables = GenerateImportVariables(context);
         var deploytools = new DeploymentScriptStep(LoadAsBase64Sql("scripts/deploytools.sql"));
         var migrations = new DeploymentScriptStep(GenerateMigrationScriptImport());

         var txstart = new DeploymentScriptStep(Load("scripts/txstart.sql"));
         var pre = new DeploymentScriptStep(Load("scripts/predeploy.sql"));
         var post = new DeploymentScriptStep(Load("scripts/postdeploy.sql"));
         var finish = new DeploymentScriptStep(Load("scripts/finish.sql"));
         var txend = new DeploymentScriptStep(Load("scripts/txend.sql"));

         // bootstrap
         AddSteps(ph, predeploy, bootstrap, mschema, variables, deploytools, migrations);

         // begin tx, run predeploy sequence
         AddSteps(ph, migrations, txstart, pre);

         // run postdeploy sequence
         AddBefore(context.PlanHandle, postdeploy, post);

         // add finishing placeholder. By default, it's NOOP but developer may use it for debugging purposes before the tx is commited.
         AddBefore(context.PlanHandle, context.PlanHandle.Tail, finish);
         // add tx closure at the end
         AddBefore(context.PlanHandle, context.PlanHandle.Tail, txend);
         // and final check to handle errors properly
         AddBefore(context.PlanHandle, context.PlanHandle.Tail,
            new DeploymentScriptStep("exec m.CleanupAndRaiseErrorOnFailure"));
      }

      void AddSteps(DeploymentPlanHandle handle, DeploymentStep after, params DeploymentStep[] steps)
      {
         var last = after;
         steps.ToList().ForEach(x =>
         {
            AddAfter(handle, last, x);
            last = x;
         });
      }

      private IEnumerable<string> LoadAsBase64Sql(string path)
      {
         return GenerateBase64Sql($"load: {path}", Load(path));
      }

      private IEnumerable<string> GenerateBase64Sql(string comment, string sql)
      {
         var result = new List<string>();
         var batches = Utils.SplitSqlBatches(sql);
         int idx = 0, total = batches.Count;
         batches.ForEach(x =>
         {
            idx++;
            var encoded = Utils.Encode(x);
            result.Add($"-- {idx}/{total} {comment}\nexec m.ExecuteBase64SQL '{encoded}'");
         });

         return result;
      }


      private IEnumerable<string> GenerateMigrationScriptImport()
      {
         var all = new List<string>();
         foreach (var dir in Directory.GetDirectories("migration"))
         {
            foreach (var file in Directory.GetFiles(dir, "*.sql"))
            {
               if (Regex.IsMatch(new FileInfo(file).Name, "^~.*"))
               {
                  continue;
               }

               var migration = File.ReadAllText(file, Encoding.UTF8);
               string[] phases = {"predeploy", "postdeploy"};
               var scripts = new List<string>();
               foreach (var phase in phases)
               {
                  var phasesql = Utils.GetMigrationScripts(migration, phase);
                  var sqlbatches = Utils.SplitSqlBatches(phasesql);
                  sqlbatches.ToList().ForEach(x =>
                  {
                     var type = new DirectoryInfo(dir).Name;
                     var name = new FileInfo(file).Name;
                     var encoded = Utils.Encode(x);
                     scripts.Add($"exec m.AddMigrationScript @type='{type}', @name='{name}', @phase='{phase}', @encoded='{encoded}'");
                  });
               }

               var joined = string.Join("\n", scripts);
               var sql = $"exec m.ExecuteBase64SQL '{Utils.Encode(joined)}'";
               all.Add($"print 'Loading SQL migration: {file}'\n{sql}");
            }
         }

         return all;

      }

      private DeploymentScriptStep GenerateImportVariables(DeploymentPlanContributorContext context)
      {
         var script = new List<string>();
         script.Add("delete from m.variables");
         context.Options.SqlCommandVariableValues.ToList().ForEach(x =>
         {
            script.Add($"insert into m.variables (name, value) values ('{x.Key}', '{x.Value}')");
         });
         script.Sort();
         return new DeploymentScriptStep(string.Join("\n", script));
      }

      private void ReportAllDrops(DeploymentPlanContributorContext context)
      {
         var ph = context.PlanHandle;
         // reporting all drops and removing drops in [m] schema
         for (var step = ph.Head; step != null;)
         {
            var next = step.Next;
            if (step is DropElementStep drop)
            {
               var name = drop.TargetElement.Name.ToString();
               if (name.StartsWith("[m]"))
               {
                  Remove(ph, drop);
               }
               else
               {
                  Warn($"Found DROP: {name}");
               }
            }

            step = next;
         }
      }

      private void Log(string msg)
         => PublishMessage(new ExtensibilityError(msg, Severity.Message));

      private void Warn(string msg)
         => PublishMessage(new ExtensibilityError(msg, Severity.Warning));

      private string Load(string path)
      {
         return $"-- {path}\n{LoadResource(path)}";
      }

      private string LoadResource(string path)
      {
         var primary = Path.Combine(Directory.GetCurrentDirectory(), path);
         if (File.Exists(primary))
         {
            Log($"Loading {primary}");
            var data = File.ReadAllText(primary, Encoding.UTF8);
            return data;
         }

         Log($"Loading embedded resource {path}");
         var assembly = Assembly.GetExecutingAssembly();
         var resource = $"{GetType().Namespace}.{path.Replace('/', '.')}";
         var stream = assembly.GetManifestResourceStream(resource)
                      ?? throw new ArgumentException($"Missing embedded resource: {resource}");
         using var reader = new StreamReader(stream, Encoding.UTF8);
         var dflt = reader.ReadToEnd();
         return dflt;
      }
   }
}

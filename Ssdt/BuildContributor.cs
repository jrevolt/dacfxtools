﻿using System.Collections.Generic;
using Microsoft.SqlServer.Dac.Deployment;
using Microsoft.SqlServer.Dac.Extensibility;

namespace JRevolt.DacFxTools.Ssdt
{
   [ExportBuildContributor("JRevolt.DacFxTools.Ssdt.DacFxToolsBuildContributor", "1.0.0.0")]
   public class DacFxToolsBuildContributor : BuildContributor
   {
      protected override void OnExecute(BuildContributorContext context, IList<ExtensibilityError> messages)
      {
         base.OnExecute(context, messages);
      }
   }
}

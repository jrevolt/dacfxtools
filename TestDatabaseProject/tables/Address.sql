CREATE TABLE [dbo].[Address]
(
  [Id] INT NOT NULL PRIMARY KEY, 
  [Street] NVARCHAR(MAX) NOT NULL, 
  [Number] VARCHAR(18) NOT NULL,
  [City] NVARCHAR(MAX) NOT NULL, 
  [ZipCode] VARCHAR(8) NOT NULL, 
  [Country] INT NOT NULL, 
  CONSTRAINT [FK_Address_Country] FOREIGN KEY ([Country]) REFERENCES [Country]([Id]), 
)

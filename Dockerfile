# syntax=docker/dockerfile:1-experimental

FROM alpine as qdh
WORKDIR /work
ADD . .
RUN find | sort && exit 1


FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine as build
WORKDIR /work
ADD . .
RUN --mount=type=cache,target=/root/.nuget/packages \
    dotnet restore -v n -s https://repo.gworks.jrevolt.io/repository/nuget/
RUN --mount=type=cache,target=/root/.nuget/packages \
    dotnet publish Main -o /app --no-restore


FROM mcr.microsoft.com/dotnet/core/runtime:3.1-alpine as runtime
ENTRYPOINT [ "dacfxtools" ]
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT false
RUN --mount=type=cache,target=/etc/apk/cache \
    --mount=type=cache,target=/var/cache/apk \
    apk add icu-libs
ADD dacfxtools.sh /usr/local/bin/dacfxtools
COPY --from=build /app/ /opt/jrevolt/dacfxtools/
#ADD out.dacpac ./
#RUN dacfxtools deploy --server=mssql,31433 --database=dacfxtools --username=ciadmin --password=ciadmin --dacpac=out.dacpac
